#include <iostream>
#include <algorithm>
#include <cstring>

#include "check_prime.hpp"

void usage(){
    std::cout << "Usage:" << std::endl;
    std::cout << "prime_checker [optinal integer]" << std::endl;
    std::cout << "Argument has to be a positive integer." << std::endl;
}

int main(int argc, char *argv[]) {
    int count, n;
    
    if (argc < 2){
        std::cout << "Enter a positive integer: ";
        std::cin >> n;
        if (!std::cin || n < 0){
            usage();
            return 1;
        }
    }else if (argc == 2 && std::all_of(argv[1], argv[1]+std::strlen(argv[1]), 
                       [](unsigned char c) { return ::isdigit(c); })){
        n = std::stoi(argv[1]);
    }
    else{
        usage();
        return 1;
    }

    is_prime(n)? std::cout << "prime" : std::cout << "not prime";
    std::cout << std::endl;

    return 0;
}