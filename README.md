# ci-with-windows10

This project contains a very basic C++ code built with cmake and tested with ctest. Gitlab-ci is then used to build, deploy and test it ona windows 10 VM. The goal of this gitlab project is to provide a didactic description of the various stages we went through to get the CI working.

Before starting you have to [enable CI/CD](https://gitlab.inria.fr/gitlabci_gallery/intro#enable-project-cicd-feature) for your project.

## Create the windows 10 VM in cloudstack

You can find all the following explainations in the [CI documentation](https://ci.inria.fr/doc/). 

1. Create a new project in https://ci.inria.fr without jenkins. The project has to be validated by an administrator, it can take 24 hours.
2. When your project is created you will recieve a e-mail syaing your project is fully created.
3. Before creating your VM make sure you copy your ssh public key in "My account" of ci.inria.fr (the menu is under your id on the top right corner).
4. Open your CI project and click on "slaves" then on "Add slave".
5. Create your windows 10 VM with the following parameters:
    * Featured template : Windows 10
    * 16 Go RAM
    * 8 cores
    * 100GB disk
    * No additionnal disk
6. When your slave is ready (running status) connect to it through the [Remote Desktop Protocol via a ssh tunnel](https://ci.inria.fr/doc/page/slaves_access_tutorial/#connecting-to-a-windows-slave). Example by using the remmina remote desktop client:
    * ssh tunnel: ci-ssh.inria.fr (SSH agent authentification)
    * RDP serveur: the name of your VM
    * RDP username: ci
    * RDP password: ci
7. Once you are connected in your windows VM you should first change the password: `Start menu/ci/change account settings/Sign-in options/Password/change`

A full documentation expalining how to use the windows 10 template with jenkins can be found [here](https://inria-ci.gitlabpages.inria.fr/doc/page/windows10-template/). It mainly uses [chocolatey](https://chocolatey.org/) as package manager. The following explanations will be based on the new windows package manager that is also available on windows 10: [winget](https://learn.microsoft.com/fr-fr/windows/package-manager/winget/).


## Install Gitlab-runner

The winget installation of Gitalb-Runner failed on our windows 10 version. So you can use Chocolatey or install by following with the official documentation.

The installation as to be donne with the Administrator privileges. To open a powershell terminal as Administrator right click on `Sart Menu/Windows PowerShell/Windows Powershell` and click on `Run as Administrator`.

### Install gitlab-runner with chocolatey

1. n a powershell terminal run as Administrator : `choco install gitlab-runner`

### Install gitlab-runner manually

1. Download 64-bit [Gitlab-Runner.exe](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) in C:\Gitlab-Runner (change the name of the downloaded file from `gitlab-runner-windows-amd64.exe` to `gitlab-runner.exe`).

### Gitlab-runner configuration and runner registration

2. In a powershell terminal run as Administrator : `gitlab-runner.exe install -u ".\ci" -d "C:\Users\ci"`
4. In the windows search toolbar type `services` and lauch it. Find `gitlab-runner` and edit the properties. In the `Log On` tab change the default password and put the ci password instead. This action has to be done manually because the `-p` option of gitlab-runner does not seem to work in windows 10... Click `apply` and `ok`. In the Services tab click on the green arrow to start the service.
5. Register your runner in your gitlab project with `.\gitlab-runner.exe register`. The URL and registration token can be found in your gitlab project under `Settings - CI/CD - Runners - Project runners`. Choose `shell` as first executor. 
6. Open the file `C:\Gitlab-Runner\config.toml` and replace `shell = "pwsh"` with `shell = "powershell"`.


## Install git:

1. Open a PowerShell terminal as Administrator.
2. Install git:
```powershell
winget install --id Git.Git -e --source winget
```

## Installation of the C++ compile chain

### Using MSYS2

1. Install MSYS2 : `winget install msys2`
2. Open a msys2 terminal in the environment you want: [list of msy2 environments](https://www.msys2.org/docs/environments/). We choose the ming64 environment for the following setps. To open the terminal use `Start menu/MSYS2/MSYS2 MINGW64` or in a powershell terminal ` C:\msys64\mingw64.exe`.
3. From the MSYS2 MINGW64 terminal:
    ```
    pacman -Sy --no-confirm mingw-w64-x86_64-gcc mingw-w64-x86_64-cmake mingw-w64-x86_64-ninja
    ```
### Using winget

In a powershell terminal:

1. gcc is not available as a winget package but it comes with Strawberryperl:
```powershell
winget install StrawberryPerl --source winget
``` 
2. install cmake, ninja and python (python is needed for gcovr)
```powershell
winget install cmake --source winget
winget install ninja --source winget
winget install python --source msstore
``` 
3. Install gcovr:
```powershell
pip install gcovr
```
and add the path of the installed python scripts. To do so copy the output of `python -c 'import os,sysconfig;print(sysconfig.get_path(\"scripts\",f\"{os.name}_user\"))'`. Open your account environment variables editor (Type `variables` in the startup menu prompt and click on `Edit environment variables for your account`). Edit the `PATH` variable and paste the path in it.

4. Restart gitlab-runner before launching any CI pipeline: 
In the windows search toolbar type `services` and lauch it. Find `gitlab-runner` and edit the properties. In the Services tab click on the green arrow to restart the service.


## Write the .gitlab-ci.yml file

We have two stages: one for building the project in a `build` folder and one for testing and getting the coverage report. See the .gitlab-ci.yml file for details.

We do not succeed to get a working coverage target with the msys2 compile chain. For gcovr, the full python installation just failed with pacman. Using lcov also causes paths problems... We stopped here our investigations. Feel free to propose a working MR if you find any solution!






